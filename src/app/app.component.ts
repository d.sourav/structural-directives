import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
 <div *ngIf="displayName; then thenBlock; else elseBlock">
 </div>

<ng-template #thenBlock>
<h2> SOURAV</h2> 
</ng-template>

<ng-template #elseBlock>
<h2> Name is Hidden</h2>
</ng-template>

<div [ngSwitch]="col">
<h3 *ngSwitchCase="'red'">you have picked red color</h3>
<h3 *ngSwitchCase="'blue'">you have picked blue color</h3>
<h3 *ngSwitchCase="'green'">you have picked green color</h3>
<h3 *ngSwitchDefault>Pick again</h3>
</div>

<div *ngFor="let color of colors;index as i; first as a;last as z">
 <h3>{{a}} {{z}} {{i}} {{color}}</h3>
 <h2>parent component</h2>
  `,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'structural-directives';
  public displayName=true;
  public col="red"
  public colors=["red","blue","green","yellow"];



}
